<?php

namespace FSM\MQAdapter;

use App\Product;

class Client
{
    protected $uri;
    protected $headers = [];
    protected $queue;
    protected $error;
    protected $errorCode;
    
    public function __construct(string $uri, string $queue, array $headers)
    {
        $this->uri = $uri;
        $this->headers = $headers;
        $this->queue = $queue;
    }
    
    public function run()
    {
        try {
            // make a connection
            $stomp = new \Stomp($this->uri);
        
            echo "Connected to ActiveMQ\n";
        
            $isSubscribe = $stomp->subscribe($this->queue);
        
            while($isSubscribe) {
                if ($stomp->hasFrame()) {
                    $frame = $stomp->readFrame();
                    if ($frame != NULL) {
                        //if ($frame->headers['type'] === '') {
                            echo date("Y-m-d H:i:s") . " - Received message:\n" . $frame->body . "\n";
                            $stomp->ack($frame);
                            $this->parseRequest($frame->body);
                        //} else {
                        //    echo date("Y-m-d H:i:s") . " - Received message has non-expected type: {$frame->headers['type']}\n";
                        //}
                    }
                }
                else {
                    print "No frames to read\n";
                }
            }

        } catch (Exception $e) {
            echo "Error: {$e->getMessage()}\n";
        } finally {
            if($isSubscribe){
                $stomp->unsubscribe($topic);
                unset($stomp);
            }
        }
    }

    protected function parseRequest(string $xml)
    {
        $simpleXml = simplexml_load_string($xml);
        
        if ($simpleXml === false)
            return '';
        
        if ($id = $simpleXml->Product->id) {
            $product = Product::find($id);
            
            if ($product) {
                return $product->asXml();
            }
        }
        
        return '';
    }

    protected function isValid($frame)
    {
        foreach ($this->headers as $key => $value) {
            if (!isset($frame->headers[$key]) || $frame->headers[$key] !== $value) {
                return false;
            }
        }
        return true;
    }
    
    public function fail()
    {
        return $this->error != '';
    }
    
    public function getError()
    {
        return $this->error;
    }
    
    public function getErrorCode()
    {
        return $this->errorCode;
    }
}
    